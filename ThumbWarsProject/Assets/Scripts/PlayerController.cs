﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private double strength = 10;
	private double speed = 10;

	private double maxstamina = 100;
	private double stamina; 
	private double regen = .2;

	private double pounceCost = 20;
	private double jukeCost = 20;

	private double pounce = 10;
	private double juke = 10;
	private double slide = 10;


	// Must double tap within half a second (by default)
	public float doubleTapTime = .75f;


	// Time that the Special Move button was last pressed
	private float _lastSpecialMoveButtonTime = 0f;

	// Time of the last Special Move
	private float _lastSpecialMoveTime = 0f;


	void Start () {
		stamina = maxstamina;
	}

	void OnGUI(){
		GUIDrawRect (new Rect (100, 320, (int)stamina * 2, 10), Color.blue);
	}


	private static Texture2D _staticRectTexture;
	private static GUIStyle _staticRectStyle;

	// Note that this function is only meant to be called from OnGUI() functions.
	public static void GUIDrawRect( Rect position, Color color )
	{
		if( _staticRectTexture == null )
		{
			_staticRectTexture = new Texture2D( 1, 1 );
		}

		if( _staticRectStyle == null )
		{
			_staticRectStyle = new GUIStyle();
		}

		_staticRectTexture.SetPixel( 0, 0, color );
		_staticRectTexture.Apply();

		_staticRectStyle.normal.background = _staticRectTexture;

		GUI.Box( position, GUIContent.none, _staticRectStyle );
	}




	void Update() {

		if (stamina < maxstamina) {
			stamina += regen; 
		}
		if (Input.anyKeyDown) {
			if (Time.time - _lastSpecialMoveButtonTime < doubleTapTime) {

				if (isSpecialMovePossible ()) {
					SpecialMove ();	
					_lastSpecialMoveTime = Time.time; 
				}
			}
			_lastSpecialMoveButtonTime = Time.time;
		}
	}
	

	bool isSpecialMovePossible(){
		return ((Time.time - _lastSpecialMoveTime) > 0.75);
	}
		
	void SpecialMove () {

		if (Input.GetKey (KeyCode.UpArrow)) {
			Pounce ();
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			Juke ("down");
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			Juke ("left");
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			Juke ("right");
		}
	}

	void Pounce(){

		if (stamina < pounceCost) {
			return;
		}

		stamina -= pounceCost;
		print ("pounce" + "     stamina remaining: " + stamina);
	}
		
	void Juke(string direction){
		
		if (stamina < pounceCost) {
			return;
		}

		stamina -= jukeCost;
		print ("juke " + direction + "     stamina remaining: " + stamina);
	}
}

	
			